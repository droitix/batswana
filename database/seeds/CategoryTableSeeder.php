<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [

                'name' => '24 hours',
                'percent' => '1.25',
                'color' => '1',
                'icon' => 'fa-laptop',
                'children' => [

                    ['name' => '25% in 24hrs '],


                     ]
            ],
            [
                'name' => '48 hours',
                'percent' => '1.5',
                'color' => '2',
                'icon' => 'fa-university',
                'children' => [
                    ['name' => '50% in 48hrs'],

                    ]
            ],
            [
                'name' => '3 days',
                'percent' => '1.7',
                'color' => '3',
                'icon' => 'fa-cogs',
                'children' => [
                    ['name' => '70% in 3days'],


                ]
            ],
            [
                'name' => '5 days',
                'percent' => '2',
                'color' => '5',
                'icon' => 'fa-cogs',
                'children' => [
                    ['name' => '100% in 5days'],


                ]
            ],
            [
                'name' => '7 days',
                'percent' => '2.2',
                'color' => '7',
                'icon' => 'fa-cogs',
                'children' => [
                    ['name' => '120% in 7days'],


                ]
            ],



        ];

        foreach ($categories as $category) {
            \openjobs\Category::create($category);
        }
    }
}
