@extends('layouts.regapp')
@section('title', 'register')

@section('description')



@endsection
@section('content')
    <div class="auth-layout-wrap" style="background-image: url(../../dist-assets/images/photo-wide-4.jpg)">
    <div class="auth-content">
        <div class="card o-hidden">
            <div class="row">
                <div class="col-md-6 text-center" style="background-size: cover;background-image: url(../../dist-assets/images/photo-long-3.jpg)">
                    <div class="pl-3 auth-right">
                        <div class="auth-logo text-center mt-4"><img src="../../dist-assets/images/logo.png" alt=""></div>
                        <div class="flex-grow-1"></div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-4">
                        <h1 class="mb-3 text-18">RESET PASSWORD</h1>
                                 <form method="POST" action="{{ route('password.email') }}">
                                @csrf
                                   <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Your email address" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                    <div class="form-group mb-0">
                                        <button class="btn btn-primary btn-block" type="submit">Reset Password</button>
                                    </div>
                                </form>
                                <!-- /Form -->

                                <div class="text-center dont-have">Remember your password? <a href="{{url('login')}}">Login</a></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
