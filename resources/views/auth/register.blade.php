@extends('layouts.regapp')
@section('title', 'register')

@section('description')



@endsection
@section('content')
    <div class="auth-layout-wrap" style="background-image: url(../../dist-assets/images/photo-wide-4.jpg)">
    <div class="auth-content">
        <div class="card o-hidden">
            <div class="row">
                <div class="col-md-6 text-center" style="background-size: cover;background-image: url(../../dist-assets/images/photo-long-3.jpg)">
                    <div class="pl-3 auth-right">
                        <div class="auth-logo text-center mt-4"><img src="../../dist-assets/images/logo.png" alt=""></div>
                        <div class="flex-grow-1"></div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-4">
                        <h1 class="mb-3 text-18">CREATE  ACCOUNT</h1>
                                 <form action="{{ route('register') }}" method="POST" autocomplete="off">
                                    @csrf

                                        <div class="form-group">
                                        <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" type="text" value="{{ old('name') }}" required autofocus  placeholder="First Name">
                                         @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                                    </div>

                                       <div class="form-group">
                                        <input class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" type="text" value="{{ old('surname') }}" required autofocus  placeholder="Last Name">
                                         @if ($errors->has('surname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                        <div class="form-group">
                                        <input class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" type="text" value="{{ old('phone') }}" required autofocus  placeholder="Cell Number">
                                         @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                                    </div>


                                <div class="form-group">

                                        <select name="bank" class="form-control">

                                        <option value="FNB">FNB</option>
                                        <option value="CAPITEC">CAPITEC</option>
                                        <option value="MUKURU">MUKURU</option>
                                        <option value="FNB-SWAZILAND">FNB-SWAZILAND</option>
                                        <option value="FNB-LESOTHO">FNB-LESOTHO</option>
                                        <option value="FNB-NAMIBIA">FNB-NAMIBIA</option>
                                        <option value="STANDARD BANK">STANDARD BANK</option>
                                        <option value="SASFIN">SASFIN</option>
                                        <option value="ABSA">ABSA</option>
                                        <option value="DISCOVERY">DISCOVERY</option>
                                        <option value="AFRICAN BANK">AFRICAN BANK</option>
                                        <option value="NEDBANK">NEDBANK</option>
                                        <option value="GROBANK">GROBANK</option>
                                        <option value="TYME BANK">TYME BANK</option>
                                        <option value="BIDVEST">BIDVEST</option>
                                        <option value="BITCOIN">BITCOIN</option>

                                        </select>
                                        </div>


                                        <div class="form-group">
                                        <input class="form-control{{ $errors->has('account') ? ' is-invalid' : '' }}" name="account" type="text" value="{{ old('account') }}" required autofocus  placeholder="Account Number">
                                         @if ($errors->has('account'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('account') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <div class="form-group">
                                       <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <div class="form-group">
                                      <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Choose a password">


                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <div class="form-group">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Type password again">
                                    </div>

                                        <div class="mt-4">
                                            <button class="btn btn-primary btn-block waves-effect waves-light" type="submit">Create Account</button>
                                        </div>




                                    </form>

                                    <div class="mt-3">
                                            <a
                                               href="{{route('login')}}" class="btn btn-primary btn-block waves-effect waves-light" type="submit">or Login</a>
                                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
