<!DOCTYPE html>
<html lang="en">
<head>

   @include('layouts.partials.head')

</head>
<body class="text-left">
  <!-- Begin page -->
        <div class="app-admin-wrap layout-sidebar-large">

   @include('layouts.partials.navigation')




       @yield('content')








</div>
 <!-- ============ Search UI End ============= -->
    <script src="/dist-assets/js/plugins/jquery-3.3.1.min.js"></script>
    <script src="/dist-assets/js/plugins/bootstrap.bundle.min.js"></script>
    <script src="/dist-assets/js/plugins/perfect-scrollbar.min.js"></script>
    <script src="/dist-assets/js/scripts/script.min.js"></script>
    <script src="/dist-assets/js/scripts/sidebar.large.script.min.js"></script>
    <script src="/dist-assets/js/plugins/echarts.min.js"></script>
    <script src="/dist-assets/js/scripts/echart.options.min.js"></script>
    <script src="/dist-assets/js/scripts/dashboard.v1.script.min.js"></script>


</body>
</html>
