<!DOCTYPE html>
<html lang="en">
<head>

   @include('layouts.partials.reghead')

</head>
<body>

          <div id="app">






       @yield('content')



           </div>





        <!-- JAVASCRIPT -->
        <script src="/assets/libs/jquery/jquery.min.js"></script>
        <script src="/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="/assets/libs/metismenu/metisMenu.min.js"></script>
        <script src="/assets/libs/simplebar/simplebar.min.js"></script>
        <script src="/assets/libs/node-waves/waves.min.js"></script>

        <!-- App js -->
        <script src="/assets/js/app.js"></script>

</body>


</html>
