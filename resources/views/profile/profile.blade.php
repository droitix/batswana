@extends('layouts.app')

@section('title', 'Profile')

@section('description')

@endsection

@section('content')


@include('layouts.partials.sidebar')

   <div class="main-content-wrap sidenav-open d-flex flex-column">
            <!-- ============ Body content start ============= -->
            <div class="main-content">
                <div class="breadcrumb">
                    <h1>Edit Profile</h1>
                    <ul>

                    </ul>
                </div>
                <div class="separator-breadcrumb border-top"></div>
                <div class="row">
                    <div class="col-md-8">

                        <div class="card mb-4">
                            <div class="card-body">
                                <form action="{{route('profile.update')}}" method="POST">
                                        @csrf
                                            <div class="row">
                                                <div class="col-sm-6">
                                                     <div class="form-group">
                                                            <label>First Name:</label>
                                                            <input type="text" class="form-control" name="name" value="{{$user->name}}">
                                                        </div>
                                                    <div class="form-group">
                                                            <label>Last Name:</label>
                                                            <input type="text" class="form-control" name="surname" value="{{$user->surname}}">
                                                        </div>
                                                     <div class="form-group">
                                                            <label>Email</label>
                                                            <input type="email" class="form-control" name="email" value="{{$user->email}}" required>
                                                        </div>
                                                   <div class="form-group">
                                                            <label>Phone:</label>
                                                            <input type="text" class="form-control" name="phone" value="{{$user->phone}}">
                                                        </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                    <label>Bank Name</label>
                                                    <input type="text" class="form-control" name="bank" value="{{$user->bank}}">
                                                </div>
                                                     <div class="form-group">
                                                    <label>Account Number</label>
                                                    <input type="text" class="form-control" name="account" value="{{$user->account}}">
                                                </div>
                                                    <div class="form-group">
                                                            <label>Branch No</label>
                                                            <input type="text" class="form-control" name="branch" value="{{$user->branch}}">
                                                    </div>
                                                    <div class="form-group">
                                                            <label>Account Type</label>
                                                            <input type="text" class="form-control" name="accounttype" value="{{$user->accounttype}}">
                                                        </div>
                                                        <input type="hidden" class="form-control" name="btcaddress" value="htfrt">
                                                </div>
                                            </div>

                                            <button type="submit" class="btn btn-primary mr-1 waves-effect waves-light">Save Changes</button>

                                        </form>

                            </div>
                        </div>
                    </div>

                </div><!-- end of main-content -->


@endsection
