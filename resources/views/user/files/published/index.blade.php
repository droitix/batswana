@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')


@include('layouts.partials.sidebar')

 <div class="main-content-wrap sidenav-open d-flex flex-column">
            <!-- ============ Body content start ============= -->
            <div class="main-content">
                <div class="breadcrumb">
                    <h1>Uploaded Proof</h1>
                    <ul>
                    </ul>
                </div>
                <div class="separator-breadcrumb border-top"></div>

                 <div class="row">
                    <div class="col-md-6">
                         <div class="card mb-5">
                            <div class="card-body">

                                       <table class="table mb-0 table-hover">
                                           <tbody>
                                     @if ($files->count())
        @each ('resumes.partials._file_own', $files, 'file')

    @else
        <tr class="mb30">
                                                <th scope="row">
                                                    <ul>
                                                        <li class="list-inline-item"><a href=""><i class="fe fe-document"></i></a></li>
                                                        <li class="list-inline-item cv_sbtitle">You have not uploaded any CV's yet <u><a href="{{route('files.upload.index')}}">UPLOAD NOW</a></u> </li>
                                                    </ul>
                                                </th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>

                                                </td>
                                            </tr>

    @endif

                                        </tbody>
                                        </table>
                            </div>
                        </div>

                <div class="border-top mb-5"></div>

                </div><!-- end of main-content -->
            </div><!-- Footer Start -->


@endsection
