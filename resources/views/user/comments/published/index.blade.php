@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')


@include('layouts.partials.sidebar')
<div class="main-content-wrap sidenav-open d-flex flex-column">
            <!-- ============ Body content start ============= -->
            <div class="main-content">
                <div class="breadcrumb">
                    <h1>My Coins</h1>
                    <ul>

                    </ul>
                </div>
                <div class="separator-breadcrumb border-top"></div>
                <!-- content goes here-->
                <section class="widget-card">
                    <div class="row">

                         @if ($comments->count())
        @each ('listings.partials._comment_own', $comments, 'comment')
        {{ $comments->links() }}
    @else
       <th>  <p style="text-align: center;">No Bids on this profile</p></th>
    @endif

                    </div>
                </section><!-- end of main-content -->
            </div><!-- Footer Start -->


@endsection
