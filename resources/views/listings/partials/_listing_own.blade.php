
@if ($listing->comments->count())

@else



<div class="col-lg-4 col-xl-4 mt-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="user-profile mb-4">
                                        <div class="ul-widget-card__user-info"><img class="profile-picture avatar-lg mb-2" src="/dist-assets/images/faces/1.png" alt="" />
                                            <p class="m-0 text-24">R {{$listing->amount}}</p>
                                            <p class="text-muted m-0">For {{$listing->category->parent->color}} Day(s)</p>
                                        </div>
                                    </div>
                                    @if (is_null($listing->updated_at))
 @php($maturitydate = \Carbon\Carbon::parse($listing->created_at->addHours($listing->category->parent->color*18)))
@else
 @php($maturitydate = \Carbon\Carbon::parse($listing->updated_at->addHours($listing->category->parent->color*18)))
@endif
                                    <div class="ul-widget-card--line mt-2">
                                        @if ($maturitydate->isPast())

<form action="{{ route('listings.update', [$area, $listing]) }}" method="post">

                                         <input type="hidden" name="category_id" value="{{ $listing->category_id }}">
                                          <input type="hidden" name="amount" class="form-control" value="{{ $listing->amount }}">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">

                                          <button type="submit" class="btn btn-primary ul-btn-raised--v2 m-1" type="button">Sell Storage</button>

                                         {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                                    </form>

@else

                                           <button class="btn btn-danger ul-btn-raised--v2 m-1" type="button">Not Mature</button>
                                       </div>
@endif
@if (session()->has('impersonate'))

<form action="{{ route('listings.update', [$area, $listing]) }}" method="post">

                                         <input type="hidden" name="category_id" value="{{ $listing->category_id }}">
                                          <input type="hidden" name="amount" class="form-control" value="{{ $listing->amount }}">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">

                                            <button type="submit" class="btn btn-outline-success ul-btn-raised--v2 m-1 float-right" type="button">Admin Sell</button>

                                         {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                                    </form>

@endif

@role('admin')

<form action="{{ route('listings.update', [$area, $listing]) }}" method="post">

                                         <input type="hidden" name="category_id" value="{{ $listing->category_id }}">
                                          <input type="hidden" name="amount" class="form-control" value="{{ $listing->amount }}">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">

                                            <button type="submit" class="btn btn-outline-success ul-btn-raised--v2 m-1 float-right" type="button">Admin Sell</button>

                                         {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                                    </form>

@endrole





                                    </div>
                                    <div class="ul-widget-card__rate-icon"><span><i class="i-Add-UserStar text-warning"></i> {{$listing->category->parent->color}} </span><span><i class="i-Bag text-primary"></i> Matures on {{$maturitydate}}</span></div>
                                </div>
                            </div>
                        </div>

          @endif
