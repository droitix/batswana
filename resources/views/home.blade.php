@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')


@include('layouts.partials.sidebar')

 <div class="main-content-wrap sidenav-open d-flex flex-column">
            <!-- ============ Body content start ============= -->
            <div class="main-content">
                <div class="breadcrumb">
                    <h1 class="mr-2">Welcome, {{Auth::user()->name}}</h1>
                    <ul>
                        <li><a href="#"></a></li>

                    </ul>
                </div>
                <div class="separator-breadcrumb border-top"></div>
                <div class="row">
                    <!-- ICON BG-->

                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                            <div class="card-body text-center"><i class="i-Financial"></i>
                                <div class="content">
                                    <p class="text-muted mt-2 mb-0">Next  Session</p>
                 @php ($sum = 0)

                 @foreach($listings as $listing)

                            @php ($sum += $listing->amount)

                           @if ($loop->last)

                           @endif



                     @endforeach

                     @php ($diff = 0)

                      @foreach($listings as $listing)


                            @foreach($listing->comments as $comment)


                            @php ($diff += $comment->split)



                            @endforeach
                      @endforeach





                                    <p class="text-primary text-24 line-height-1 mb-2">R {{number_format($sum-$diff,1)}}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4"><a href="{{ route('comments.published.index') }}">
                            <div class="card-body text-center"><i class="i-Checkout-Basket"></i>
                                <div class="content">
                                    <p class="text-muted mt-2 mb-0">My Coins</p>
                                     <p class="text-primary text-24 line-height-1 mb-2">R 0</p>
                                    
                            </div>
                             </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4"><a href="{{ route('listings.unpublished.index', [$area]) }}">
                            <div class="card-body text-center"><i class="i-Money-2"></i>
                                <div class="content">
                                    <p class="text-muted mt-2 mb-0">Maturing Coins</p>
                                     <p class="text-primary text-24 line-height-1 mb-2">R 0</p>
                                 
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>


            <div class="flex-grow-1"></div>
            <div class="app-footer">
                <div class="row">
                    <div class="col-md-9">
                        <p><strong>Our Coins Auction Sessions Are 9am and 7pm Daily</strong></p>
                        <p>
                            <sunt></sunt>
                        </p>
                    </div>
                </div>
                <div class="footer-bottom border-top pt-3 d-flex flex-column flex-sm-row align-items-center">
                    <a class="btn btn-primary text-white btn-rounded" href="{{url('bidding')}}">Buy Coins Now</a>
                    <span class="flex-grow-1"></span>
                    <div class="d-flex align-items-center">
                        <img class="logo" src="../../dist-assets/images/logo.png" alt="">
                        <div>
                            <p class="m-0">&copy; 2023 Auction Club</p>
                            <p class="m-0">All rights reserved</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- fotter end -->
        </div>
    </div><!-- ============ Search UI Start ============= -->



@endsection
