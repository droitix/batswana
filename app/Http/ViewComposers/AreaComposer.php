<?php

namespace openjobs\Http\ViewComposers;

use Illuminate\View\View;

class AreaComposer
{
    private $area;

    public function compose(View $view)
    {
        if (!$this->area) {
            $this->area = \openjobs\Area::where('slug', session()->get('area', config()->get('openjobs.defaults.area')))->first();
        }

        return $view->with('area', $this->area);
    }
}
