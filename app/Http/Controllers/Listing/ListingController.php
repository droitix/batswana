<?php

namespace openjobs\Http\Controllers\Listing;

use openjobs\{Area, Category, Listing};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use openjobs\Http\Controllers\Controller;
use openjobs\Jobs\UserViewedListing;
use openjobs\Http\Requests\StoreListingFormRequest;
use RealRashid\SweetAlert\Facades\Alert;


class ListingController extends Controller
{
    public function index(Category $category)
    {
        $listings = Listing::with(['user', 'area'])->isLive()->fromCategory($category)->latestFirst()->paginate(15);

        $areas = Area::withListings()->get()->toTree();

        return view('listings.index', compact('listings', 'category','areas'));

    }

     public function inarea(Area $area)
    {
        $listings = Listing::with(['user', 'area'])->isLive()->InArea($area)->latestFirst()->paginate(15);
           $areas = Area::withListings()->get()->toTree();




        return view('listings.inarea', compact('listings', 'area', 'areas'));

    }

      public function allsa(Area $area, Category $category, Listing $listing)
    {
         $listings = Listing::with(['area'])->isLive()->latestFirst()->paginate(15);

         $categories = Category::withListings()->get()->toTree();

         $areas = Area::withListings()->get()->toTree();



        return view('listings.allsa', compact('listings','categories','areas'));
    }



    public function show(Request $request, Area $area, Listing $listing)
    {
        $listings = Listing::with(['area'])->isLive()->paginate(10);

        if (!$listing->live()) {
            abort(404);
        }

        if ($request->user()) {
            dispatch(new UserViewedListing($request->user(), $listing));
        }

        return view('listings.show', compact('listing','listings'));
    }

     public function apply(Request $request, Area $area, Listing $listing, Category $category)
    {

         $categories = Category::get()->toTree();

        if (!$listing->live()) {
            abort(404);
        }

        if ($request->user()) {
            dispatch(new UserViewedListing($request->user(), $listing));
        }

        return view('listings.apply', compact('listing','categories'));
    }

     public function bid(Request $request, Area $area, Listing $listing, Category $category)
    {

         $categories = Category::get()->toTree();

        if (!$listing->live()) {
            abort(404);
        }

        if ($request->user()) {
            dispatch(new UserViewedListing($request->user(), $listing));
        }

        return view('listings.bid', compact('listing','categories'));
    }

     public function applicants(Request $request, Area $area, Listing $listing)
    {
        if (!$listing->live()) {
            abort(404);
        }

        if ($request->user()) {
            dispatch(new UserViewedListing($request->user(), $listing));
        }

        return view('listings.applicants', compact('listing'));
    }

    public function create()
    {
        return view('listings.create');
    }

    public function bcreate()
    {
        return view('listings.bcreate');
    }



    public function store(StoreListingFormRequest $request, Area $area)

    {
        $listing = new Listing;
        $listing->amount = $request->amount;
        $listing->maturityamount = $request->maturityamount;
        $listing->secret = $request->secret;
        $listing->type = $request->type;
        $listing->updated_at = $request->updated_at;



        $listing->category_id = $request->category_id;
        $listing->area_id = $request->area_id;

        $listing->user()->associate($request->user());

        $listing->live = false;
        $listing->save();

        return redirect()
            ->route('listings.unpublished.index', [$area, $listing])
            ->with('success', 'Bid Stored!');
    }

    public function edit(Request $request, Area $area, Listing $listing)
    {
        $this->authorize('edit', $listing);

        return view('listings.edit', compact('listing'));
    }

    public function update(StoreListingFormRequest $request, Area $area, Listing $listing)
    {
        $this->authorize('update', $listing);

         $listing->maturityamount = $request->maturityamount;

        if (!$listing->live()) {
            $listing->category_id = $request->category_id;

        }

        $listing->area_id = $request->area_id;







        $listing->live = true;
        $listing->created_at = \Carbon\Carbon::now();
        $listing->save();



        return redirect()
            ->route('listings.published.index', [$area, $listing])
            ->withSuccess('Congratulations! Your coins are now selling.');
    }


      public function disable(StoreListingFormRequest $request, Area $area, Listing $listing)
    {
        $this->authorize('disable', $listing);

         $listing->maturityamount = $request->maturityamount;

        if (!$listing->live()) {
            $listing->category_id = $request->category_id;

        }

        $listing->area_id = $request->area_id;







        $listing->live = false;
        $listing->created_at = \Carbon\Carbon::now();
        $listing->save();



        return redirect()
            ->route('listings.published.index', [$area, $listing])
            ->withSuccess('Congratulations! Your coins are now selling.');
    }

    public function destroy(Area $area, Listing $listing)
    {
        $this->authorize('destroy', $listing);

        $listing->delete();

        return back()->withSuccess('Listing was deleted.');
    }
 /**
     * Search for listings by category and keywords.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request, Area $area)
    {
         $areas = Area::withListings()->get()->toTree();

        $keyword = $request->get('query');


        if(empty($keyword))
        {
            //we'll return an empty listing collection if no category or keyword
            $listings = collect(new Listing);
            return view('search')->with(compact('listings','keyword','areas'));
        }







        $listings = Listing::SearchByKeyword($keyword)->paginate(20);

        return view('search')->with(compact('listings','keyword','areas'));
    }

}
