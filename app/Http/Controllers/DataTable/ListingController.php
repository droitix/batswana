<?php

namespace openjobs\Http\Controllers\DataTable;

use openjobs\{Area, Category, Listing};
use Illuminate\Http\Request;
use openjobs\Http\Controllers\DataTable\DataTableController;

class ListingController extends DataTableController
{


    public function builder()
    {
        return Listing::query();
    }

    public function getDisplayableColumns()
    {
        return [
            'id',
            'user_id',
            'jobtitle',
            'companyname',



        ];
    }


     public function getUpdatableColumns()
    {
        return [
            'user_id',
            'address',
            'companyname',
            'jobtitle',

        ];
    }

      public function update($id, Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'companyname' => 'required',

        ]);

        $this->builder->find($id)->update($request->only($this->getUpdatableColumns()));
    }

}
