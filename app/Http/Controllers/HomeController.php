<?php

namespace openjobs\Http\Controllers;

use openjobs\{Area, Category, Listing, Comment};
use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
      const INDEX_LIMIT = 10;

    public function index(Area $area, Category $category, Listing $listing, Comment $comment)
    {
         $listings = Listing::all()->where('live',true);

         $unpublishedcount = Auth::user()->listings()->where('live', false)->count();




         $referralink = 'https://golix.app/register/?ref=' . \Hashids::encode(auth()->user()->id);


        return view('home', compact('listings','unpublishedcount','referralink'));
    }

    public function referral()
  {

  }

  public function referrer()
{
    return auth()->user()->referrer;
}

public function referrals()
{
    $referrals = auth()->user()->referrals()->paginate(20);

     return view('referrals', compact('referrals'));
}

}
